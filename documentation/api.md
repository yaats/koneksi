# Koneksi Restfull WebAPI

## Basic content building blocks

- Section
- Snippet
- Content blocks (text, reference)

A section can contain other sections and snippets (text or reference).

## Actions

- / - GET
- /sections - GET,POST
- /sections/:id - GET,PUT,DELETE
- /snippets - GET,POST
- /snippets/:id - GET,PUT,DELETE

## JSON-Schema's examples

### GET /

Response code 200, example body:

```json
[{
  "_id": "01",
  "language": "en",
  "name": "Main section",
  "sections": [
    {
      "_id": "02",
      "name": "Subsection",
      "language": "en",
    }
  ],
  "parent": null
}]
```

### POST /sections

Example request body:

```json
{
  "name": "Main section",
  "language": "en"
}
```

Example request body:

```json
{
  "parent": "O3",
  "index": "1",
  "name": "Subsection",
  "language": "en"
}
```

### GET /section/:id

Response code 200, example body:

```json
{
  "_id": "03",
  "name": "Section name",
  "language": "en",
  "snippets": [
    {
      "_id": "06",
      "model": "text",
      "language": "en",
      "value": {
        "text": "This is text"
      }
    }
  ],
  "section": [
    {
      "_id": "04",
      "name": "Section",
      "language": "en"
    }
  ],
  "parent": {
    "_id": "02",
    "name": "Parent name",
    "language": "en",
    "parent": {
      "_id": "01",
      "language": "en",
      "name": "Main section"
    }
  }
}
```

### UPDATE /sections/:id

Example request body:

```json
{
  "_id": "0123",
  "parent": "O3",
  "index": "1",
  "name": "Subsection",
  "language": "en",
  "createdAt": "201908040000",
  "updateddAt": "201908040000"
}
```

Response code 200, with example body:

```json
{
  "_id": "0123",
  "parent": "O3",
  "index": "1",
  "name": "Subsection",
  "language": "en",
  "createdAt": "201908040000",
  "updateddAt": "201908050200"
}
```

### DELETE /sections/:id

Response code 204

### GET /snippets

```json
[{
  "_id": "01",
  "language": "nl",
  "model": "text",
  "snippetModel": "Text",
  "value": {
    "_id": "0101",
    "value": "text",
     "language": "en",
    "createdAt": "201908040000",
    "updateddAt": "201908050200"
  },
  "createdAt": "2019-08-02T20:11:24.685Z",
  "updatedAt": "2019-08-02T20:11:24.685Z",
}]
```

### POST /snippets

Example request body:

```json
{
  "index": "01",
  "language": "nl",
  "model": "text",
  "value": "Dit is tekst."
}
```

Response code 200, example body:

```json
{
  "_id": "123",
  "index": "01",
  "language": "nl",
  "model": "text",
  "value": "Dit is tekst."
}
```

### DELETE /snippets/:id

Response code 204
