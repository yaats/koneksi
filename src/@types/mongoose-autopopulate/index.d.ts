declare module 'mongoose-autopopulate' {
  import { Schema } from 'mongoose'
  export default function (schema: Schema): void
}
