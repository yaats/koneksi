import Debug from 'debug'
import { NextFunction, Request, Response } from 'express'
const debug = Debug('app:controllers:helpers')

const errorHandler = (err: Error, req: Request, res: Response, next: NextFunction) => {
  // debug(err)

  res.status(500).json({
    message: err.message
  })
}

const notFoundHandler = (req: Request, res: Response, next: NextFunction) => {
  res.status(404).json({ error: 'Resource not found' })
}

export const helpers = { errorHandler, notFoundHandler }
