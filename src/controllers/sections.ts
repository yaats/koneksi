import Debug from 'debug'
import { NextFunction, Request, Response } from 'express'
import { Section } from '../models/section'

const debug = Debug('app:controllers:sections')

const get = (req: Request, res: Response, next: NextFunction) => {
  Section.find({})
    .populate('snippets')
    .then((result) => {
      res.json(result)
    })
    .catch((err) => {
      debug(err)
    })
}

const getMain = (req: Request, res: Response, next: NextFunction) => {
  Section.find({ parent: null || undefined })
    .populate('snippets')
    .then((result) => {
      res.json(result)
    })
    .catch((err) => {
      next(err)
    })
}

const post = async (req: Request, res: Response, next: NextFunction) => {
  const errorHandler = (err: Error) => {
    debug(err)
    next(err)
  }

  // Create new section
  const newSection = new Section(req.body)

  // Save new section
  const createdSection = await newSection.save().catch(errorHandler)
  if (!createdSection) return

  if (req.body.parent) {
    // Get parent section
    const parent = await Section.findById(req.body.parent).exec().catch(errorHandler)
    if (!parent) return

    // Update parent section
    if (req.body.index) {
      parent.childs.splice(req.body.index, 0, createdSection._id)
    } else {
      parent.childs.push(createdSection._id)
    }

    // Save updated parent
    const updatedParent = parent.save().catch(errorHandler)
    if (!updatedParent) return
  }

  res.json(createdSection)
}

const getById = (req: Request, res: Response, next: NextFunction) => {
  Section.findById(req.params.id)
    .then((result) => res.json(result))
    .catch((err) => {
      next(err)
    })
}

const update = async (req: Request, res: Response, next: NextFunction) => {
  const errorHandler = (err: Error) => {
    debug(err)
    next(err)
  }
  const item = await Section.findById(req.params.id).exec().catch(errorHandler)
  if (!item) return next()

  item.name = req.body.name
  item.language = req.body.language

  const updatedItem = await item.save().catch(errorHandler)
  if (!updatedItem) return next()

  res.json(item)
}

// - Remove section
// - Update parent section
const removeById = async (req: Request, res: Response, next: NextFunction) => {
  const errorHandler = (err: Error) => {
    debug(err)
    next(err)
  }

  // Get item
  const item = await Section.findById(req.params.id).exec().catch(errorHandler)
  if (!item) return next()

  // Remove item
  const removedIten = await item.remove().catch(errorHandler)
  if (!removedIten) return next()

  // Update parent by removing section from childs
  if (item.parent) {
    // Get parent
    const parent = await Section.findById(item.parent).exec().catch(errorHandler)
    if (!parent) return

    // Update parent
    parent.childs.remove(item._id)

    // Save parent
    const updatedParent = parent.save().catch(errorHandler)
    if (!updatedParent) return
  }

  res.json(removedIten)
}

export const section = { get, getById, getMain, post, removeById, update }
