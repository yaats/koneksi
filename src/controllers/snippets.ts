import Debug from 'debug'
import { NextFunction, Request, Response } from 'express'
import { IReference } from '../interfaces/reference'
import { IText } from '../interfaces/text'
import { IReferenceModel, Reference } from '../models/reference'
import { Section } from '../models/section'
import { Snippet } from '../models/snippet'
import { ITextModel, Text } from '../models/text'

const debug = Debug('app:controllers:snippets')

const get = (req: Request, res: Response, next: NextFunction) => {
  Snippet.find({})
    .then((result) => {
      res.json(result)
    })
    .catch((err) => {
      debug(err)
    })
}

const createText = (textSnippet: IText): Promise<ITextModel> => {
  const newText = new Text(textSnippet)
  return newText.save()
}

const createReference = (referenceSnippet: IReference): Promise<IReferenceModel> => {
  const newReference = new Reference(referenceSnippet)
  return newReference.save()
}

// 1. Create value document (text)
// 2. Create snippet document
// 3. Update section snippets array
const post = async (req: Request, res: Response, next: NextFunction) => {
  const errorHandler = (err: any) => {
    debug(err)
    next(err)
  }

  // Create value document
  const textDocument = {
    language: req.body.language,
    snippet: req.body.snippet,
    value: req.body.value
  }

  const savedText = await createText(textDocument).catch(errorHandler)
  if (!savedText) return

  // Convert text to Text, reference to Reference
  const model = req.body.model[0].toUpperCase() + req.body.model.substring(1)

  // Create snippet document
  const snippetDocument = {
    body: savedText._id,
    section: req.body.section,
    snippetModel: model
  }

  const newSnippet = new Snippet(snippetDocument)
  const savedSnippet = await newSnippet.save().catch(errorHandler)
  if (!savedSnippet) return

  // Get section
  const section = await Section.findById(req.body.section).exec().catch(errorHandler)
  if (!section) return

  // Update section (add snippet to section.snippets)
  if (req.body.index) { section.snippets.splice(req.body.index, 0, savedSnippet._id) } else {
    section.snippets.push(savedSnippet._id)
  }

  // Save section
  const savedSection = await section.save().catch(errorHandler)
  if (!savedSection) return

  res.json(savedSnippet)
}

const getById = async (req: Request, res: Response, next: NextFunction) => {
  const errorHandler = (err: Error) => {
    debug(err)
    next(err)
  }

  const item = await Snippet.findById(req.params.id).exec().catch(errorHandler)
  debug(item)
  if (!item) return next()

  res.json(item)
}

const removeById = async (req: Request, res: Response, next: NextFunction) => {
  const errorHandler = (err: Error) => {
    debug(err)
    next(err)
  }

  const item = await Snippet.findById(req.params.id).exec().catch(errorHandler)
  if (!item) return next()

  const removedItem = await item.remove().catch(errorHandler)
  if (!removedItem) return

  res.status(204).end()
}

const update = async (req: Request, res: Response, next: NextFunction) => {
  const errorHandler = (error: Error) => {
    debug(error)
    next(error)
  }

  const item = await Snippet.findById(req.params.id).exec().catch(errorHandler)
  if (!item) return next()

  const valueItem = item.body as ITextModel

  valueItem.language = req.body.language
  valueItem.value = req.body.value

  const savedValueItem = await valueItem.save().catch(errorHandler)
  if (!savedValueItem) return next()

  res.json(valueItem)
}

export const snippet = { get, getById, post, removeById, update }
