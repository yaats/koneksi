import bodyParser from 'body-parser'
import Debug from 'debug'
import express from 'express'
import mongoose from 'mongoose'
import routes from './routes'

const debug = Debug('app:index')

// Create a new express application instance
const app: express.Application = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

// https://mongoosejs.com/docs/deprecations.html
mongoose.set('useNewUrlParser', true)

mongoose.connect('mongodb://localhost:27017/koneksi')
  .then(() => {
    debug('Connected to mongoDB')
  })
  .catch((err) => { throw err })

app.use('/', routes)

app.listen(3000, () => {
  debug('Koneksi listening on port 3000')
})
