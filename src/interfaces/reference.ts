import { Types } from 'mongoose'

export interface IReference {
  label: string
  language: string
  section: Types.ObjectId
}
