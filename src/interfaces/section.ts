import { Types } from 'mongoose'
import { ISectionModel } from '../models/section'
import { ISnippetModel } from '../models/snippet'

export interface ISection {
  language: string
  name: string
  parent: Types.ObjectId
  childs: Types.DocumentArray<ISectionModel>
  snippets: Types.DocumentArray<ISnippetModel>
}
