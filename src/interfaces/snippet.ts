import { Types } from 'mongoose'
import { ITextModel } from '../models/text';

export interface ISnippet {
  section: Types.ObjectId
  snippetModel: string
  body: ITextModel | Types.ObjectId
}
