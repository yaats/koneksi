export interface IText {
  language: string
  snippet: string
  value: string
}
