import { Document, Model, model, Schema } from 'mongoose'
import mongooseAutopopulate from 'mongoose-autopopulate'
import { IOrganization } from '../interfaces/organization'

export interface IOrganizationModel extends IOrganization, Document { }

const schema = new Schema({
  __v: { type: Number, select: false },
  name: String
})

/** Enable createAt and updatedAt */
schema.set('timestamps', true)

/** Use mongoose-autopopulate plugin */
schema.plugin(mongooseAutopopulate)

export const Organization: Model<IOrganizationModel> = model<IOrganizationModel>('Organization', schema)
