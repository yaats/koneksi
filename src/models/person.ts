import { Document, Model, model, Schema } from 'mongoose'
import mongooseAutopopulate from 'mongoose-autopopulate'
import { IPerson } from '../interfaces/person'

export interface IPersonModel extends IPerson, Document { }

const schema = new Schema({
  __v: { type: Number, select: false },
  name: String
})

/** Enable createAt and updatedAt */
schema.set('timestamps', true)

/** Use mongoose-autopopulate plugin */
schema.plugin(mongooseAutopopulate)

export const Person: Model<IPersonModel> = model<IPersonModel>('Person', schema)
