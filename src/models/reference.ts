import { Document, Model, model, Schema } from 'mongoose'
import mongooseAutopopulate from 'mongoose-autopopulate'

import { languages } from '../settings/languages'

import { IReference } from '../interfaces/reference'

export interface IReferenceModel extends IReference, Document { }

const schema = new Schema({
  __v: { type: Number, select: false },
  label: String,
  language: {
    enum: languages,
    required: true,
    type: String
  },
  section: { ref: 'Section', type: Schema.Types.ObjectId }
})

/** Enable createAt and updatedAt */
schema.set('timestamps', true)

/** Use mongoose-autopopulate plugin */
schema.plugin(mongooseAutopopulate)

export const Reference: Model<IReferenceModel> = model<IReferenceModel>('Reference', schema)
