import { Document, Model, model, Schema } from 'mongoose'
import mongooseAutopopulate from 'mongoose-autopopulate'

import { languages } from '../settings/languages'

import { ISection } from '../interfaces/section'

export interface ISectionModel extends ISection, Document { }

const schema = new Schema({
  __v: { type: Number, select: false },
  childs: [{
    autopopulate: { maxDepth: 1 },
    ref: 'Section',
    type: Schema.Types.ObjectId
  }],
  language: {
    enum: languages,
    required: true,
    type: String
  },
  name: [String],
  organization: { ref: 'Organization', type: Schema.Types.ObjectId },
  parent: {
    autopopulate: true,
    ref: 'Section',
    type: Schema.Types.ObjectId
  },
  snippets: [{
    ref: 'Snippet',
    type: Schema.Types.ObjectId
  }]
})

/** Enable createAt and updatedAt */
schema.set('timestamps', true)

/** Use mongoose-autopopulate plugin */
schema.plugin(mongooseAutopopulate)

export const Section: Model<ISectionModel> = model<ISectionModel>('Section', schema)
