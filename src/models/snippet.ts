import { Document, Model, model, Schema } from 'mongoose'
import mongooseAutopopulate from 'mongoose-autopopulate'
import { ISnippet } from '../interfaces/snippet'

export interface ISnippetModel extends ISnippet, Document { }

const schema = new Schema({
  __v: { type: Number, select: false },
  body: {
    autopopulate: true,
    refPath: 'snippetModel',
    required: true,
    type: Schema.Types.ObjectId
  },
  organization: { ref: 'Organization', type: Schema.Types.ObjectId },
  section: {
    ref: 'Section',
    type: Schema.Types.ObjectId
  },
  snippetModel: {
    default: 'Text',
    enum: ['Reference', 'Text'],
    required: true,
    type: String
  },
})

/** Enable createAt and updatedAt */
schema.set('timestamps', true)

/** Use mongoose-autopopulate plugin */
schema.plugin(mongooseAutopopulate)

export const Snippet: Model<ISnippetModel> = model<ISnippetModel>('Snippet', schema)
