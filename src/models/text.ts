import { Document, Model, model, Schema } from 'mongoose'
import mongooseAutopopulate from 'mongoose-autopopulate'

import { languages } from '../settings/languages'

import { IText } from '../interfaces/text'

export interface ITextModel extends IText, Document { }

const schema = new Schema({
  __v: { type: Number, select: false },
  language: {
    enum: languages,
    type: String
  },
  organization: { ref: 'Organization', type: Schema.Types.ObjectId },
  snippet: {
    ref: 'Snippet',
    type: Schema.Types.ObjectId
  },
  value: String
})

/** Enable createAt and updatedAt */
schema.set('timestamps', true)

/** Use mongoose-autopopulate plugin */
schema.plugin(mongooseAutopopulate)

export const Text: Model<ITextModel> = model<ITextModel>('Text', schema)
