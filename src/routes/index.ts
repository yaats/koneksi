import express from 'express'
const router = express.Router()

// Import routes
import organizations from './organizations'
import persons from './persons'
import sections from './sections'
import snippets from './snippets'

// Import controller
import { helpers } from '../controllers/helpers'
import { section } from '../controllers/sections'

// Set routes
router.use('/organizations', organizations)
router.use('/persons', persons)
router.use('/sections', sections)
router.use('/snippets', snippets)

router.route('/')
.get(section.getMain)

router.use(helpers.notFoundHandler)

export default router
