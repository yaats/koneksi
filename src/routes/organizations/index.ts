// Sections router
import express from 'express'

import { helpers } from '../../controllers/helpers'

const router = express.Router()

router.route('/')
.get()
.post()

router.route('/:id')
.get()
.put()
.delete()

router.use(helpers.errorHandler)

export default router
