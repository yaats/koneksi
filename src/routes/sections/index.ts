// Sections router
import express from 'express'

import { helpers } from '../../controllers/helpers'
import { section } from '../../controllers/sections'
import { validateIdParam } from '../../validators/helper'
import { validateSectionPost, validateSectionPut } from '../../validators/section'

const router = express.Router()

router.route('/')
.get(section.get)
.post(validateSectionPost, section.post)

router.route('/:id')
.all(validateIdParam)
.get(section.getById)
.put(validateSectionPut, section.update)
.delete(section.removeById)

router.use(helpers.errorHandler)

export default router
