// Snippets router
import express from 'express'

import { helpers } from '../../controllers/helpers'
import { snippet } from '../../controllers/snippets'
import { validateIdParam } from '../../validators/helper'
import { validateSnippetPost, validateSnippetPut } from '../../validators/snippet'

const router = express.Router()

router.route('/')
.get(snippet.get)
.post(validateSnippetPost, snippet.post)

router.route('/:id')
.all(validateIdParam)
.get(snippet.getById)
.put(validateSnippetPut, snippet.update)
.delete(snippet.removeById)

router.use(helpers.errorHandler)

export default router
