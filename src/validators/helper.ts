import * as Joi from '@hapi/joi'
import { NextFunction, Request, Response } from 'express'
import { Types } from 'mongoose'

import Debug from 'debug'
const debug = Debug('app:validators:helper')

export const validateIdParam = (req: Request, res: Response, next: NextFunction) => {
  const validObjectId = Types.ObjectId.isValid(req.params.id)
  debug('validObjectId', validObjectId)
  if (!validObjectId) return next(new Error('Invalid ObjectId'))

  next()
}
