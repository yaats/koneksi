import * as Joi from '@hapi/joi'

export const ObjectIdSchema = Joi.string().hex()
