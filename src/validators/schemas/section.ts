import * as Joi from '@hapi/joi'
import { languages } from '../../settings/languages'

const post = Joi.object().keys({
  index: Joi.number(),
  language: Joi.string().valid(languages),
  name: Joi.string(),
  parent: Joi.string().hex()
})

const putExt = Joi.object().keys({
  __v: Joi.any().strip(),
  _id: Joi.any().strip(),
  createdAt: Joi.any().strip(),
  updatedAt: Joi.any().strip()
})

const put = post.concat(putExt)

export const sectionSchemas = { post, put }
