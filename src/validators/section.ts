import * as Joi from '@hapi/joi'
import Debug from 'debug'

import { NextFunction, Request, Response } from 'express'
import { sectionSchemas } from './schemas/section'

const debug = Debug('app:validators:section')

export const validateSectionPost = (req: Request, res: Response, next: NextFunction) => {
  const validated = Joi.validate(req.body, sectionSchemas.post)

  if (validated.error) {
    debug(validated)
    return next(validated.error)
  }

  req.body = validated.value

  next()
}

// Validate req.body
export const validateSectionPut = (req: Request, res: Response, next: NextFunction) => {

  // Validate req.body
  const validated = Joi.validate(req.body, sectionSchemas.put)

  // Stop execution if invalid
  if (validated.error) {
    debug(validated)
    return next(validated.error)
  }

  req.body = validated.value

  next()
}
