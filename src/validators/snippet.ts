import * as Joi from '@hapi/joi'
import Debug from 'debug'
import { NextFunction, Request, Response } from 'express'
import { snippetSchemas } from './schemas/snippet';

const debug = Debug('app:validators:snippet')

export const validateSnippetPost = (req: Request, res: Response, next: NextFunction) => {
  const validated = Joi.validate(req.body, snippetSchemas.post)

  if (validated.error) {
    debug(validated)
    return next(validated.error)
  }

  req.body = validated.value

  next()
}

export const validateSnippetPut = (req: Request, res: Response, next: NextFunction) => {
  const validated = Joi.validate(req.body, snippetSchemas.put)

  if (validated.error) {
    debug(validated)
    return next(validated.error)
  }

  req.body = validated.value

  next()
}
